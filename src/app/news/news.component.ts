import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { userInfo } from 'os';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  constructor(private firestore:AngularFirestore) { }
  private user=sessionStorage.getItem("current_user");
  private date=Date();
  private descriptio ="";
  
  Check(){
    this.descriptio=document.getElementById("descrip").nodeValue;
    var News = {
      userName : this.user,
      dateNow : this.date,
      description : this.descriptio
    }
    
    this.firestore.collection("News").add(News);
    alert('successful !')
  }
  ngOnInit(): void {
  }

}
