import { Component, OnInit } from '@angular/core';
import { CasesService } from '../cases.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(public casesService: CasesService) { }

  ngOnInit(): void {
  }

}

