import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from './user.model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CasesService {
  
  private user : User | undefined; 

  constructor(private afAuth: AngularFireAuth, private router:Router) { }
  async signinwithGoogle() {
    const credientials =await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider);
    if (credientials.user === null ){
      alert('no user logged in');
    }else{ 
    this.user= {
    uid: credientials.user.uid,
    displayName: credientials.user.displayName,
    email: credientials.user.email
    };
    if (this.user.displayName!==null){
    sessionStorage.setItem("current_user",this.user.displayName);
  }
    this.router.navigate(["homepage"]);
  }
  }}