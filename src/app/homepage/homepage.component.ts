import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  
  constructor(private router: Router) {}
  News(){
    this.router.navigate(['news'])
  }
  World(){
    this.router.navigate(['worldwide'])
  }
  Countries(){
    this.router.navigate(['Countries'])

  }

  ngOnInit(): void {
  }

}
