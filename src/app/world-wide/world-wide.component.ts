import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {AngularFirestore} from '@angular/fire/firestore';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, MultiDataSet } from 'ng2-charts';


@Component({
  selector: 'app-world-wide',
  templateUrl: './world-wide.component.html',
  styleUrls: ['./world-wide.component.css']
})
export class WorldWideComponent implements OnInit {
  private L1=[];
  private L2=[]
  constructor(private apiService: ApiService, private firestore : AngularFirestore){
    //L=[Number(this.data["TotalConfirmed"]), Number(this.data["TotalDeaths"]), Number(this.data["TotalRecovered"])];
  
  }
  
 
  data=[];
  ngOnInit() {
    this.apiService.getStats()
    ._subscribe( data => {
      try{
      this.data=data['body']['Global'];
      this.firestore.collection("Global").add(this.data);
      console.log(Number(this.data["TotalConfirmed"]));
      this.L1.push( Number(this.data["TotalConfirmed"]), Number(this.data["TotalDeaths"]), Number(this.data["TotalRecovered"]));
      this.L2.push( Number(this.data["NewConfirmed"]), Number(this.data["NewDeaths"]), Number(this.data["NewRecovered"]));}
      catch{
        alert('error')
      }
      
    }); 
  };
  
  doughnutChartLabels: Label[] = ['TotalConfirmed', 'TotalDeaths', 'TotalRecovered'];
  doughnutChartData: MultiDataSet = [this.L1
  ];
  doughnutChartType: ChartType = 'doughnut';
  
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ['NewConfirmed', 'NewDeaths', 'NewRecovered'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: this.L2, label: 'New Cases Statistics' }
  ];
}
