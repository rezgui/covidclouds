import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  constructor(private apiService: ApiService, private firestore : AngularFirestore){
    
  }
  data=[];
  ngOnInit() {
    this.apiService.getStats()
    ._subscribe( data => {
      this.data=data['body']['Countries']; 
      localStorage.setItem("Countries",data['body']['Countries']);
      
      this.firestore.collection("Countries").add(Object.assign({},data['body']['Countries']));
      
    })   
  }

}
