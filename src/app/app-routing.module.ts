import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import {WorldWideComponent} from './world-wide/world-wide.component'
import {HomepageComponent} from './homepage/homepage.component'
import { CountriesComponent } from './countries/countries.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {NewsComponent} from './news/news.component'

const routes: Routes = [
  { path:"signin", component: SigninComponent},
  { path:"homepage", component: HomepageComponent},
  { path:"worldwide", component: WorldWideComponent},
  { path:"Countries", component: CountriesComponent},
  { path:"news", component: NewsComponent},
  { path: "**", component: PageNotFoundComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
