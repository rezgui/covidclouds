import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import {  SigninComponent } from './signin/signin.component';
import { WorldWideComponent } from './world-wide/world-wide.component';
import { HttpClientModule} from '@angular/common/http';
import { HomepageComponent } from './homepage/homepage.component';
import { CountriesComponent } from './countries/countries.component'
import { ChartsModule } from 'ng2-charts';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NewsComponent } from './news/news.component';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    WorldWideComponent,
    HomepageComponent,
    CountriesComponent,
    PageNotFoundComponent,
    NewsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    HttpClientModule,
    ChartsModule,
    


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
