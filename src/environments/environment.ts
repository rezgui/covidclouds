// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBZ0WsvCbj3XczktFay-pYaUC19JN4_vGQ",
    authDomain: "covidforclouds-695cb.firebaseapp.com",
    projectId: "covidforclouds-695cb",
    storageBucket: "covidforclouds-695cb.appspot.com",
    messagingSenderId: "976837293042",
    appId: "1:976837293042:web:79a23059924712c091b16b",
    measurementId: "G-4L9JL7JFSP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
